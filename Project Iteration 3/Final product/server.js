var http = require('http');
var fs = require('fs');
var index = fs.readFileSync('TeamDrive.html');
var dataPage = fs.readFileSync('dataPage.html');
var port = 8000;

http.createServer(function (req, resp) {
    switch (req.method) {
        case ("GET"):
            if (req.url === '/') {
                resp.write(index);
                resp.end();

            } else if (req.url === '/dataPage'){
                resp.write(dataPage);
                resp.end();
            }
            break;
    };
}).listen(port);
