var http = require('http');
var fs = require('fs');
var index = fs.readFileSync('inspector.html');
var filePage = fs.readFileSync('filePage.html');
var dataPage = fs.readFileSync('dataPage.html');
var port = 8000;

/*
http.createServer(function(req,res){
    res.write(index);
    res.end();
}).listen(8000);
*/

http.createServer(function (req, resp) {
    switch (req.method) {
        case ("GET"):
            if (req.url === '/') {
                resp.write(index);
                resp.end();

            } else if (req.url === '/filePage') {
                resp.write(filePage);
                resp.end();
            } else if (req.url === '/dataPage'){
                resp.write(dataPage);
                resp.end();
            }
            break;
        case("POST"):
            break;
    };
}).listen(port);
